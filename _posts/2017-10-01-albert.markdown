---
layout: post
title:  "Man vs Robot"
teasertitle: "Man vs Robot"
teaser: "Automated machines are disrupting labour markets and threatening mass unemployment. European labour unions are getting creative as they fight back."
date:   2017-10-01 12:00:57
author: Albert Guasch Rafael
authorimg: AlbertGuaschRafael.jpg
twitter: albertguaschr
hostwriter: albert-guasch
collaboration: "Catia and Alun threw in many ideas and references while I was still brainstorming the details of the piece. Overall, everyone brought up possible sources in their respective countries and articles that would be relevant to my topic. As stories, like our world, become more global, journalism must benefit from cross-border collaboration."
summary: Albert is a Spanish journalist based in Barcelona (though currently in NYC for the UN). He’s got a knack for everything European, particularly its economy and its institutional design.
permalink: /article/albert-rafael-guasch_man-vs-robot
---

<h2 class="article-intro">Automated machines are disrupting labour markets and threatening mass unemployment. Now European labour unions, representing a range of workforces from shipping ports to international banks, are fighting back.</h2>

For anyone worried a dystopian future run by robots is not far off, a visit to the newly automated terminals at the Port of Rotterdam is ill advised.

> The whole process, which requires little human intervention, turns the Port into an automated and precise 3D game of Tetris

In the fully automated APM Terminals, automated cranes unload containers from some of the world’s largest international trade vessels straight on to driverless vehicles. The vehicles then drive themselves along predefined lanes and stack the containers ready to be picked up by other cranes which load them on to trains and trucks for distribution across Europe. The whole process, which requires little human intervention, turns the Port into an automated and precise 3D game of Tetris.

The robots don’t just run the Port of Rotterdam; automation is rolling out in other sectors worldwide. We stand at the doors of the fourth Industrial Revolution. 

Just how disruptive this Industrial Revolution will be is still uncertain. A study from the University of Oxford suggests 47 percent of jobs in the United States are at risk of being automated, whereas research from the Organisation for Economic Cooperation and Development (OECD) predicts a moderate 9 percent job loss in developed countries.

Georgios Petropoulos, an economist from think tank Bruegel says automation is not yet prevalent enough to measure its impact. But, he says, “The use of automated systems has increased a lot (since the 1990s) and we predict that it will quickly increase in the future.”

As well as the Ports, fintech – an industry with technological disruption central to its business model –  might offer some clues as to how this next Industrial Revolution will impact labour markets.

###A test case of automation 

On April 2015, Kim Fejfer, CEO of the APM terminals declared, “We are pleased to welcome the future here today.” But for the workers at the port, <em>the future today</em> was no cause for celebration. 

They feared that unless the port achieved its expected growth, cargo would be diverted to the new, lower cost, automated terminals and port authorities would close the man-staffed terminals. Dutch dockers’ union, FNV Havens, demanded a guarantee that all jobs would be kept until 2024. The Port operators called the Union’s demands “something from another era.” But time proved them apt for this new era of automation.

“In the beginning we were told the transformation of the terminals would create new jobs,” says Niek Stam, Head of FNV Havens, “but the evidence is that it destroyed them.” 

> “Our unions are at the forefront of this debate because automation has been happening for many years in our industry.”

Union negotiations resulted in a guarantee for all 3,700 employees until July 2020, but those guarantees didn’t mean full job security given technology reduced the labour requirements of the port. The average age of workers at the port (54 years) meant that an early retirement scheme reduced the workforce and employees over 60 were offered to work 60 percent of full-time hours at 95 percent of their salary. 

The Port also agreed that an internal corridor slated to open in 2018 would be man operated, to retain jobs. This corridor, which was initially planned to be automated, connects the terminals so that a crane can be diverted to another country – Britain, for example – without going through customs.   

“Ports are at the forefront of automation,” says Olaf Merk, a shipping expert with the OECD. “People are only now excited about self-driving trucks, but don’t seem to realise that some ports have had autonomous trucks for a few decades. Containerisation has led to more standardised processes that can be more easily automated,” he explains.

Stam adds, “In the past a crane team would consist of seven people. Now it is around two or three workers because the rest of the tasks have been automated. Our unions are at the forefront of this debate because automation has been happening for many years in our industry.”

Tony Burke, Assistant General Secretary at Unite, Britain’s biggest union, sees automation affecting a broad range of labour workforces. He warns that affected workers don’t always have powerful unions behind them. Specifically, many workers that perform routine jobs in the services sector or in manufacturing, which have a big automation potential, are under temporary contracts that offer little protection and are less unionized. 

“Let’s say that a company with lots of temporary workers wants to introduce a new technology. These workers are among the first to be laid off, because (the companies) simply do not renew their contract,” says Burke.

In a report published in 2015, the OECD found that the richest 10 percent of the population earned 9.6 times the income of the poorest 10 percent, partly attributing the divide to employment conditions. Many fear that as the introduction of automation accelerates, rampant unemployment could further widen the gap. But higher income jobs aren’t immune to the threats of automation.

###A cushy job turns risky

A few years ago, white collar ‘knowledge’ jobs were thought to be safe from technological take over. You can automate a factory line, we thought, but surely not a law firm.  But law and banking – industries that often require years of study, emotional intelligence and a strong professional network – are no longer as safe as they once seemed. 

> …higher income jobs aren’t immune to the threats of automation.

Technology is innovating every aspect of the industry: apps are taking over customer service; peer-to-peer lending is taking lending power to the masses; and bitcoin might even replace traditional currencies. These are just a few of the innovations that are snatching bits and pieces of the market – and jobs – from the finance industry. On top of that, a hazy economic environment is not helping job stability. 

And although some financial institutions are holding their own when it comes to technological innovation, that doesn’t necessarily translate to jobs. In October last year ING announced plans to invest €800 million in their digital transformation, a measure that would entail annual savings of €900 million, as well as 7000 layoffs. 
While the job cuts spread across the bank’s operations, most of the layoffs affected people in retail banking. As customers turn online for their financial operations, the bank had less need for counter staff and physical branches. Nearly half of ING’s branches are set for closure as part of the bank’s digital transformation.  
Herman Vanderhaegen, a leader at Belgium union LBC-NVK, says the layoffs are as much a question of technology as the result of unrealistic promises on the bank’s stakeholders on their return in investment. 

“On a legal basis, layoffs caused by technology are the same as for other reasons,” he says, “but the staff had a very emotional reaction and they were even furious (because) they recognised that technology was only one of the factors in their redundancies.”

In Belgium, more than 3,500 of ING’s 9,000 employees were expected to leave the company in October 2016. By March this year, the Union reached an agreement with ING allowing workers over 55 years old to resign with a pay that ranges from 60 percent to 80 percent of their former salary until the age of their pension. Younger workers are also financially incentivised to leave the bank. 

The Union is still negotiating with ING but Vanderhaegen is confident that it can bring down the number of compulsory layoffs to 450. “ING is a very profitable company and thus they must pay for the social impact caused by their organisation,” he says.

Regardless of the final number of lay-offs, there is a lesson for the industry: banking is not a safe haven from automation-related layoffs. According to Vanderhaegen, people who are leaving ING are seeking jobs in other sectors.  

“It is not that easy to find another job when you have worked 15 or 20 years in banking. This is why we offer career counselling where workers can reassess what they want to do or what skills they have,” says Elke Maes, National Secretary at LBC-NVK. 

“We have to accept that in some sectors working 9:00 to 5:00 is not normal anymore. We provided training to our union representatives and drafted documents and brochures on what we can or cannot accept in negotiations in this new scenario,” Maes says.

###Opportunities may await

Len McCluskey, Secretary General of Unite, called on the United Kingdom’s government to set up a commission last March to examine the growing threat of automation in the economy, naming the motor industry, manufacturing and the service sector as the most prone to automation. 

McCluskey says automation could be a good thing, if handled well. “It should be an opportunity for a shorter working week with no loss in pay, or the gateway to a nationwide programme of re-skilling and up-skilling existing workers, while also creating new training and apprenticeship schemes,” he says. 

The British government has not yet acted on Unite’s calls. “At the moment, the debate is being left to academics, think tanks and unions. The Labour Party listens to our concerns but equally they have no answers,” Burke explains. 

Meanwhile Unite knows the challenge of protecting union members against job loss from technology is only going to grow. “We know it is going to happen and that it is not something we can stop,” Burke says. 

Stam, of the FNV Havens Union also worries about that automation might have an impact on the welfare state. Less people employed could translate into lower state revenues through income tax. Compensating the loss through corporate taxes is not an option, he says, given the different creative accounting schemes that allow companies to elude corporate taxes. He argues, “In a fair society, if robots take over our jobs, then they should also take the obligation to pay taxes.” 

> To (Bill) Gates, the robot tax would also help slow down the pace of automation so that we could better manage its effects

A tax on robots has some well-known proponents, namely Microsoft’s founder Bill Gates. Even the European Parliament briefly considered such a tax to fund retraining schemes for workers affected by technology, but ultimately rejected it. To Gates, the robot tax would also help slow down the pace of automation so that we could better manage its effects. 

A universal basic income is also gaining traction in forums and think tanks worldwide as a way of mitigating the social consequences of technological unemployment. 

All of this will depend of course on the extent to which automation ends up replacing jobs. 

When ATMs were introduced in the 1970’s, the same question loomed over bank tellers: was their job disappearing? But ATMs made it cheaper to run a bank, so more branches could open. In fact, the number of tellers hired increased, but their tasks changed to that of selling financial products, something humans still do much better than machines.      

Whichever the outcome, it is undeniable that some jobs will be lost on the way. As the pace of automation accelerates and we start seeing the effects, unions will need to step up their efforts. After all, no union wants to come across as a luddite these days. Quite simply, as Tony Burke says, “you cannot uninvent electricity.” 
