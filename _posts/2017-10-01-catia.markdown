---
layout: post
title:  "Nostalgia wards off populism in Portugal"
teasertitle: "Nostalgia wards off populism"
teaser: "There is no room for populism in Portugal. But with working class towns battling unemployment, poverty and isolation, will the post-authoritarian apathy last?"
date:   2017-10-01 12:00:58
author: Cátia Bruno
authorimg: CatiaBruno.jpg
twitter: catiabruno
hostwriter: cátia-bruno
collaboration: "Everyone gave me input on specific regions of their country that I could use to compare with the regions of Portugal that I picked. Alun and Jelena’s contributions proved to be ideal comparison for my piece."
summary: Cátia is a Portuguese journalist with a focus on international stories, whether that’s covering Portugal for international publications, or reporting on international news for Portuguese media.
links:
  - title: "Published in DeCorrespondent "
    url: "http://www.example.com"
    flag: eu
permalink: /article/catia-bruno_nostalgia-wards-off-populism-in-portugal
---

<h2 class="article-intro">Disaffected working-class communities across Europe are increasingly lending their support to populist parties. But not so in Portugal where working class towns battling unemployment, poverty and isolation remain politically apathetic.</h2>

Death is part of life in Caxinas.

Xico Flores remembers a knock at his door. There was a storm that night; his brother-in-law’s boat had gone down at sea. “Some guys tried to swim there and help them,” he told me, “but they got scared and came back.” On that night almost thirty years ago, Xico’s brother, his brother-in-law and three other men died at sea.

It only takes around 20 minutes to cross this small village by foot, but it is described as the largest fishing community in the country. For many years, the sea has provided for entire families here. The men work on the boats and the women work in canned fish factories or help in the “obligation” (a job tailored for women, who help unload their husbands’ boats and take it to auction in return for 25 percent of profits). 

Xico started working when he was 12 years old. His wife, Lena first walked through the factory gates when she was 11 years old.

 <figure>
   <img src="{{ site.baseurl }}/img/IMG_2058.jpg" alt="Xico and Lena in their living room in Caxinas.">
   <figcaption>Xico and Lena in their living room in Caxinas. (Cátia Bruno)</figcaption>
 </figure>

“Forty-two years of work and all she gets is a three-hundred-and-something euro pension,” Xico says. Lena looks at her hands, thickened by years of removing fish spines with tweezers, washing fillets, dipping them in olive oil and packing them.

In 1964, after her factory demanded workers should come in on a Sunday, Lena joined her colleagues in a strike. In a country under a conservative dictatorship, it was a brave action. The next day, she was singled out with a red stamp on her company's card. 

Such political gestures are harder to find nowadays. In the last parliamentary elections, only 51 percent of voters in Caxinas’ parish (Vila do Conde) went to the polling booths and the ones who did gave solid support to the centrist parties. The Social Democratic Party (PSD) received 38.5 percent of votes in Vila do Conde and the Socialist Party (PS) came second with 34.8 percent.

“Most people don’t care about politics,” Xico says, “The ones who do are those who try to screw others by sucking up to the shipmasters. And the shipmasters only care about themselves and how to fill up their own pockets.” 

###But don’t mess with the Church…

Politics becomes more popular when the church enters the mix.  Two years ago, a petition to stop the construction of a tall building next to the local church gathered more than 2,500 signatures. In a small community that has lost more than 100 men to the sea in the last 35 years, faith and worship are very important. 

Manuel Moreira, who worked in the sea all his life, opens the church door gleaming with pride. It is impossible not to notice the tattoo on his left forearm, a depiction of Christ on the cross. And it’s impossible not to notice the large ship’s helm installed next to the church altar.

 <figure>
   <img src="{{ site.baseurl }}/img/IMG_2028.jpg" alt="A graffittied wall in Caxinas pays homage to the sea.">
   <figcaption>A graffittied wall in Caxinas pays homage to the sea. It reads &#39;Sea, Memory, Identity. &#39; (Cátia Bruno)</figcaption>
 </figure>

“When this issue with the church and the building happened, the mayor called us thugs,” Manuel says, “That’s a harsh word. We may be fishermen and we may speak loudly, but that’s just the way we talk.” City Hall rejected the petition and Manuel predicts many will turn their backs on the PS in the next election. 

But that doesn’t necessarily mean there will be a big election upset. Most locals say they plan to either abstain or to vote for the other largest party, PSD. But the issue of the church might be one of many seeds that could grow in to political engagement in Caxinas.

The low wages in Caxinas have pushed many fishermen to try their luck in boats far away in Hamburg and Asturias. Meanwhile fishermen from poorer countries have tried to find work in Portugal (Ukrainians in the past and Indonesians more recently). The numbers of foreign fishermen is still low but it’s not clear how they might influence political outcomes in the village. Reactions to foreigners in this small community often hit a cultural barrier. “I remember when I worked in a boat in Algeria,” Xico tells me. “Those Muslims would stop in the middle of the working day and they would start praying, with their asses up in the air,” he says, puzzled.

For now, though, the new development next to the Our Lady of Seafarers church is a more pressing issue in Caxinas. To everything else, most Caxineiros just react with a shrug of their shoulders.

###An isolated village, dug up and left out to dry

There is resignation by the coast, but there is also resignation inland, on Portugal’s border with Spain. The unemployment rate in Mourão, the municipality Aldeia da Luz belongs to, is currently around 19 percent, way over the national average of 10 percent. During the height of the economic crisis (2011-2014) it reached almost 23 percent.

In a region where vineyards and olive trees are the main economic support, it is hard to find many young people. Rute Vidigal is one of the exceptions. At 30 years old, this environmental engineer has moved back with her husband and her daughter to the place where she grew up. She found work in a dried fruits and nuts factory close by, in Mourão, where she is in charge of product quality. 

But the Aldeia da Luz where I met Rute isn’t the same as where she was born. This one was built in 2002 after the town was submerged to make room for the Alqueva Dam which, at 250 square kilometers, is the largest artificial lake in Europe. 

 <figure>
   <img src="{{ site.baseurl }}/img/IMG_2024.jpg" alt="A street in the “new” Aldeia da Luz.">
   <figcaption>A street in the “new” Aldeia da Luz.(Cátia Bruno)</figcaption>
 </figure> 

The moving process from the old village to the new one was far from peaceful. Rute, who was 14 years old at the time, remembers when the old cemetery was dug up and remains were moved. “Every time a body was transferred…it was like a second funeral all over again. The whole village would walk behind the funeral car,” she tells me.

The hastened move brought discomfort to Luz population, but there was no big upheaval like what happened for example, in Capel Celyn in Wales in the 1960s. The flooding of that village to build a reservoir for Liverpool is often credited as one of the main sparks that ignited the Welsh nationalist movement. In Luz, there was no such revolt. Opposition to the move consisted only of one small protest and complaints behind closed doors. 

“This is like a wound in your hand,” João Correia, 57 year old bus driver, tells me about the town’s upheaval. “At first it bleeds a lot, then it forms a bit of a scab and you start to forget about it,” he says. Nowadays João prefers to focus on other things, like his son’s return from Angola — a former colony where he found work as an engineer— or the duck stew he is about to share with some friends. 

Still, he shares some thoughts about what he felt was a lack of support by City Hall during the move. “Politics over here is a love den. Politicians are moved by personal interests,” he says. João votes in each election, like most of his neighbours. He won’t say which party he votes for but he can’t help but boast that Luz is one of the few PSD supporting parishes in the region. 

 <figure>
   <img src="{{ site.baseurl }}/img/IMG_2019.jpg" alt="João Correia enjoys a duck stew lunch with his friends in a local restaurant in Aldeia da Luz.">
   <figcaption>João Correia enjoys a duck stew lunch with his friends in a local restaurant in Aldeia da Luz. (Cátia Bruno)</figcaption>
 </figure> 

The trauma of the move might have faded but fifteen years hasn’t healed all wounds. “I still dream about the old village”, Rute says, “And I know my dreams take place there and not on this new Luz, because the bullfighting arena is the old one – the square one.” The new one, with 500 seats that rarely sell out, is round.

###Longing for a toxic past

Four hundred kilometers north from Luz, Urgeiriça is where the only uranium mines ever built in Portugal once thrived. It’s a small village made up of little white houses shadowed by the major headframe of one of the old mining pits.

The few hundred residents of Urgeiriça are nostalgic for the old days; the days when kids played on the mine heap – the “Yellow Mountain," they called it, because of the way the uranium tinged their hair. They think fondly of the company that used to provide a living wage, a house, free water, electrical light and wood for each worker. The same company responsible for work conditions that had them inhaling toxic dusts that would later bring widespread silicosis and cancer.

The night I visit, a local theater group stages a walking play through the old mines. In one scene, an actor hangs on the headframe and starts singing the Urgeiriça miners’ anthem. The locals – who up until then had been nervously giggling throughout the performance – join him, singing their hearts out. The theatre troupe leads us to the canteen and serves us like workers celebrating the holiday for Santa Bárbara, the patron saint of the miners. A lady dishes out caldo verde (a typical Portuguese soup made with chorizo and thinly shredded cabbage) straight from a plastic bucket.

 <figure>
   <img src="{{ site.baseurl }}/img/434006.jpg" alt="A local theatre group stages a walking play through the old mines of Urgeiriça">
   <figcaption>A local theatre group stages a walking play through the old mines of Urgeiriça. In a particularly intense scene, a miner is killed. (Cátia Bruno)</figcaption>
 </figure> 

Olívia Cardoso is a former cleaner turned accountant for the mining company, who joined the play as an extra. She remembers the real Santa Bárbara celebrations fondly. “I remember the snack they gave us, when I was a child,” she tells me, “We would get six biscuits and chocolate milk. I have never found chocolate milk that tasted like that.” 

The holiday is celebrated in mining communities throughout Europe, from Urgeiriça to, for example, Hénin-Beaumont, in France. That mining town elected one of the first Front National mayors in all of France and in May gave Marine Le Pen her first seat in the National Assembly. Just like in Hénin-Beaumont, the people in Urgeiriça feel forgotten by the rest of their country. However, instead of voting for more radical parties, they stick to the mainstream centrist options available in Portugal: the PSD and the PS.

 <figure>
   <img src="{{ site.baseurl }}/img/434165.jpg" alt="A local theatre group stages a walking play through the old mines of Urgeiriça">
   <figcaption>A local theatre group stages a walking play through the old mines of Urgeiriça. Theatregoers are served as though they were celebrating the holiday for Santa Bárbara, the patron saint of the miners. (Cátia Bruno)</figcaption>
 </figure> 

“I listen to some of the things Le Pen says and I get it, she is right about some stuff. But then there are other things that I can’t stomach. They are human beings!” Olívia tells me, talking about migrants. Even though she describes the Portuguese as “tolerant”, she is afraid of what might happen if an influx of migrants and refugees reaches Portugal. “The fascist lingo touches people’s hearts,” she says, “particularly in the poor and deserted regions.”

When Olívia was 16 years old she cleaned up the chapel after two workers were autopsied there. Her father-in-law, a miner, died due to the silicosis deeply entrenched in his lungs. Before she’d even reached her 50s, Olívia went through chemotherapy to deal with a tumor on one of her breasts.

But it’s still easy to find people in Urgeiriça who dismiss the connection between uranium and cancer. They say the campaign to get compensation was a political scheme organized by António Minhoto, a former worker who led the fight for compensation through the Workers of the Urgeiriça Mines Association (the ATMU) and who is closely connected to a left-wing party. The ATMU estimates more than 100 workers have died from the disease. 

Olívia is not sure of what the future may bring. She worries deeply about her two sons, who have left their mining village to work as engineers for big companies. They can’t seem to find the life-work balance to get married and have children of their own. “The companies just suck everything up,” she says, fiddling with the golden pendant around her neck. 

Manuel Moreira has a similar nervous look as he sits by the sea in front of the Caxinas church, telling me about his daughter who got her degree but couldn’t find a job as a kindergarten teacher.

João Correia also spends sleepless nights in his house on the new Aldeia da Luz, thinking about his son who can’t bring money from Angola to Portugal due to the local oil crisis.

These three share the same worries about their children’s future. But they also share a similar political resignation. “We are just tiny little ants,” Olívia says. “We might as well focus on just minding our own business.”
