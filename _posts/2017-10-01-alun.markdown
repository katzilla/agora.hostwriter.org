---
layout: post
title:  "Who will save Lady Liberty?"
teasertitle: "Who will save Lady Liberty?"
teaser: "The UK's justice system is under threat. We look at the safeguards in place to protect it and look abroad to see what happens when they are eroded. "
date:   2017-10-01 12:00:53
author: Alun Macer-Wright
authorimg: AlunMacerWright.jpg
twitter: alunmw221
hostwriter: alun-macer-wright
collaboration: "I think the principles the project explored have huge potential as a means of identifying trends which go beyond borders and creating a more informed public debate. As more people cross borders worldwide, so should journalism."
summary: Alun is from Wales and a first year student at City University of London. He is the youngest Agora Project participant, which is why we paired him up with Andrea in Armenia.
permalink: /article/alun-macer-wright_who_will_save_lady_liberty
---

<h2 class="article-intro">With the United Kingdom’s legal system facing upheaval through the Brexit process, we take stock of the safeguards the British justice system has in place, and look abroad to see what happens when safeguards are eroded.</h2>

The United Kingdom’s legal system is one of the oldest and most respected in the world, but in recent years austerity cuts have taken their toll.

Since the Conservative Party returned to office in 2010, Legal Aid has been slashed. The Legal Aid, Sentencing and Punishment of Offenders (LASPO) Act 2012 introduced stricter eligibility criteria for those seeking help and removed whole chunks of law (including employment cases that don’t involve human trafficking or a breach of the Equality Act) from eligibility. As a result, in 2013 Legal Aid was granted in 46 percent fewer cases than the previous year – a drop of 428,000 cases.

Amnesty International says austerity measures are creating a two-tier justice system. “The cuts are having the biggest effect on poorer people and all this does is entrench injustice,” according to Rachel Logan, barrister and Law & Human Rights Programme Director for Amnesty International UK. “We have a legal system which is adversarial, and if both parties don’t have equal access to resources then that’s a major problem,” she says.

A parliamentary review of LASPO was announced in January this year. But the review’s timetable was scrapped when June’s snap general election ended the former parliament. Logan expects the review will ultimately go ahead but warns, “a lot of time in this parliament could be taken up by Brexit legislation, which could make it more difficult to pass other laws.”

> We have a legal system which is adversarial, and if both parties don’t have equal access to resources then that’s a major problem.

LawWorks is a charity that connects volunteer lawyers with people who need legal assistance but don’t meet the new Legal Aid criteria. James Sandbach, Director of Policy and External Affairs at LawWorks has noticed an increase in people looking for pro bono help as an alternative to Legal Aid since LASPO was introduced in 2012.

“The government’s claims that the genuinely needy can still get help don’t stand up to scrutiny at all. By definition, if you take out a whole area of law from being eligible for Legal Aid then the poorest are going to lose out,” Sandbach says.

Sandbach recognises that rationing of resources is necessary, but describes cuts to legal advice as counterproductive. “Resources aren’t being used properly,” he says, “Around a third of the criminal Legal Aid budget goes on a handful of cases. The money could be distributed more evenly.”

He suggests a system that supports legal assistance in the early stages of a dispute would reduce the number of cases that proceed to expensive litigation.

Sandbach believes the current Legal Aid policy is short sighted and blames a lack of solid legal backgrounds among figures in politics and government. He says, “Policy makers have been less inclined to see the value of access to legal advice in recent years.”

His criticism is not unique. Prior to the Constitutional Reform Act 2005, the Lord Chancellor (or Justice Secretary) was almost always a trained lawyer plucked from the House of Lords. Nowadays, the position is usually filled by a more junior politician with little legal background. 

> Policy makers have been less inclined to see the value of access to legal advice in recent years.

Chris Grayling, who held the post between 2012 and 2015 was described by leading barrister Lord Pannick QC as, “notable only for his attempts to restrict judicial review and human rights, his failure to protect the judiciary against criticism from his colleagues; and the reduction of Legal Aid to a bare minimum.”

Liz Truss, the first female Lord Chancellor, drew the ire of England and Wales’ most senior judge, the Lord Chief Justice, over her perceived failure to defend the judiciary against press attacks after the Daily Mail carried on the front page of its 4 November 2016 edition the headline, ‘Enemies of the People’ alongside pictures of three Supreme Court judges who had voted to require parliamentary consent before the Brexit process began.  In response to statements by Truss in which she stressed the importance of press freedom, the Lord Chief Justice said, “I can understand how the pressures were on her in November, but she has taken a position that is constitutionally, absolutely wrong.”

> Constitutional changes have been a major issue elsewhere in the EU as well

With the process of exiting the European Union presenting an unprecedented change to the UK’s laws and constitution, uncertainty and upheaval look set to continue. Constitutional changes have been a major issue elsewhere in the EU as well, with member states pushing the EU's rules on domestic justice systems to their limit.

In Poland the Law and Justice Party (PiS) government is at loggerheads with the EU over controversial judicial reforms being pushed by Jarosław Kaczyński, the head of PiS. The proposed laws sought to put the judicial system under the government’s control, giving government the ability to hire and retire judges at will.

Following protests across the country, President Andrzej Duda announced on July 24 that he would veto the reforms. In a televised address, President Duda said, “These laws must be amended (because they) would not strengthen the sense of justice in society”. His announcement was a blow to PiS and the first time the President had deviated from supporting Kaczyński. 

Maciej Nowicki, Deputy Chair of the Management Board at the Helsinki Foundation for Human Rights (HFHR), an NGO based in Poland, says even without the laws proposed by Kaczyński’s, the legal situation in Poland is under threat. “The government is moving away from the liberal democracy adopted by the 1997 Constitution of Poland,” he says.

“The PiS-dominated parliament has adopted a number of unconstitutional laws while at the same time destructing judicial review by paralysing the Constitutional Tribunal, so that the judiciary could not block the unconstitutional reform,” Nowicki explains.

Nowicki says the Constitutional Tribunal is no longer fit for purpose. “By now it is not an effective remedy for human rights violations, and in our (HFHR) strategic litigation, we hope that the European Court of Human Rights at Strasbourg will acknowledge that,” she says.

The EU’s message to Poland has been strongly worded, and before President Dudu’s veto announcement, Vice President of the European Commission Frans Timmermans said the Commission was “very close” to invoking Article 7 of the EU Constitution. Article 7 would suspend Poland’s voting rights at the Council of the EU in response to a ‘breach of fundamental rights.’  That measure has not yet been used against a member state but was considered in a similar context in 2012 when the Hungarian government forced some judges in to early retirement.

> On public television, judges are commonly referred to as a ‘clique’ and a ‘caste.’

Like in the UK, Nowicki says the Polish government’s changes to the judicial system were accompanied by attack on judges in the press. He says about the situation in Poland, “(There is) a smear campaign against judges by the politically controlled public media... On public television, judges are commonly referred to as a ‘clique’ and a ‘caste.’”

While press attacks and political movements against the justice system are going unchecked in both Poland and the UK, the judiciary is forced in to an increasingly uncertain environment. Although the situation in Poland is more severe, it serves as warning to the UK and other mature democracies about the value of an independent judiciary and the safeguards in place to protect it.
