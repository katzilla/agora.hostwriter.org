---
layout: minimal
title: Agora 2020
permalink: /AgoraEuropeGrants2020/
---

<div class="apply">
  <section class="info post-content">
    <h3>Agora Europe Journalism Grants 2020</h3>
    <p class="intro">Emergency Fund for Freelance Journalists in Eastern and Southern Europe<br>
      <b>Deadline: 23:00 (Central European Time), 22 June 2020</b>
    </p>
    <h3>Eligibility Requirements:</h3>
    <p class="intro">This is a grant opportunity for journalists from Eastern and Southern Europe. Only journalists based in these countries are eligible to apply.
      <a href="#countries">*</a> Please read all info before submitting your application.
    </p>
    <h3>Background</h3>
    <p class="intro">
      The Coronavirus pandemic has brought considerable human suffering and major economic disruption. The crisis is also hitting journalism and poses an existential threat to freelance journalists in Europe and around the world. Some countries have recognized
      the negative impact on the free press and have implemented support measures. However, in Eastern and Southern Europe, such support measures are currently glaringly absent.<br><br> Simultaneously, there is a significant risk that countries will
      use the emergency situation of the pandemic to further retreat from their obligations to protect human rights and ensure press freedom.<br><br> Hostwriter sees cross-border journalism as a tool to solve two pressing global challenges: to raise the
      quality of foreign reporting and to diversify the Western-centric news narrative. By connecting international media professionals, providing training in "Cross-Border Journalism" and launching projects that shed light on unequal power dynamics within
      the field, Hostwriter powers the collaborative future of journalism that is as diverse as the societies it serves.
    </p>
    <h3>Funding Opportunity:</h3>
    <p class="intro">
      To address these issues, Hostwriter is offering funding in the form of grants to journalists based in Eastern and Southern Europe through the Agora Europe Journalism Grants Program. <br><br> 
      Applicants must pitch a single story that they see missing in current news coverage. Additionally, they should share information about their
      current professional situation and how the Coronavirus pandemic has specifically affected their work as a journalist. This information will enable us to understand the scope of the current crisis in the media sector in different parts of Europe.<br><br>
      <b>The Agora Europe Journalism Grants Program will consist of two parts:</b><br><br>
      <b>Part I </b> – Emergency Support Funding for Individual Journalists<br> Twenty (20) journalists will be chosen from the pool of applicants. Each journalist will receive €500 in direct support funding. This funding may be used by the recipient as
      they see fit (rent, living expenses, food, etc.). In return, they agree to participate in Part II of the Grants Program.<br><br>
      <b>Part II </b>- Agora Europe Reporting Project Funds A second pool of €3000 in funding will be available to split among 5-10 projects. Following an online kick-off meeting, journalists will be grouped into teams based on their proposed project topics.
      Journalists will work together within these teams to produce collaborative journalism stories.<br><br> The reporting process will be complimented with digital capacity-building activities, mentoring and opportunities to connect with publishers through
      our network.<br><br>
      <i>Participants will be expected to publish their stories locally and/or regionally.</i><br><br> The stories will be translated into English and published in an online dossier by Hostwriter. The online dossier will be released in a digital launch
      event in late-2020, with a presentation and an open Q&A session with the reporting teams, where lessons from their collaboration will be shared with a wider community of journalists.
    </p>
    <p id="countries">
      * Countries of eligibility: Eastern Europe (Belarus, Bulgaria, Czechia, Hungary, Poland, Republic of Moldova, Romania, Russian Federation, Slovakia, Ukraine). Southern Europe (Albania, Andorra, Bosnia and Herzegovina, Croatia, Gibraltar, Greece, Holy
      See, Italy, Malta, Montenegro, North Macedonia, Portugal, Republic of Kosovo, San Marino, Serbia, Slovenia, Spain).<br><br>
    </p>
  </section>

  <h1>The applications are now closed.</h1><br><br><br>
  <!-- <section id="submit" class="pad-lg light-gray-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2" style="text-align: center">
          <i class="fa fa-check-circle margin-40"></i>
          <h2>Apply Now:</h2>
          <form method="post" action="/formsubmit/formmail.php" style="text-align: left">
            <label>Full name:<input class="form-control" type="text" name="Name" placeholder="" type="text" required></label>
            <label>Country you are based in:<input class="form-control" type="text" name="Country" placeholder="" type="text" required></label>
            <label>Email address:<input class="form-control" type="text" name="Mail" placeholder="" type="email" required></label>
            <label>Story Pitch:<textarea class="form-control" type="text" name="Pitch" placeholder="Pitch your story idea as you would to an editor (maximum 100 words)" cols="50" rows="8" required></textarea></label>
            <label>Please provide a link to a recent work sample that has your name on it:<input class="form-control" type="url" name="Work sample" placeholder="" required></label><br>
            <label><b>Please answer the following questions, so that we may assess the challenges journalists are facing during the COVID-19 crisis:</b></label><br><br>
            <label>I have faced losses of income due to the Coronavirus crisis (choose one):</label>
            <div class="form-check form-check-inline">
              <input type="radio" id="incomelossagree" name="Faced losses of income" value="agree">
              <label for="incomelossagree">Agree</label>
              <input type="radio" id="incomelossdisagree" name="Faced losses of income" value="disagree">
              <label for="incomelossdisagree">Disagree</label>
            </div><br>
            <label>I am considering leaving the journalism profession due to the impact of COVID-19 (choose one):</label>
            <div class="form-check form-check-inline">
              <input type="radio" id="leavingagree" name="Considering leaving the journalism profession" value="agree">
              <label for="leavingagree">Agree</label>
              <input type="radio" id="leavingdisagree" name="Considering leaving the journalism profession" value="disagree">
              <label for="leavingdisagree">Disagree</label>
            </div><br>
            <div class="form-group">
            <label for="sel1">What would help you the most to overcome this situation? (choose one)</label>
            <select name="What would help" class="form-control" id="sel1">
              <option value="Assignments">Assignments</option>
              <option value="Contacts">Contacts to publishers</option>
              <option value="Funds">Direct emergency funds</option>
              <option value="Training">Training</option>
            </select>
          </div>
          <label>Your professional situation:<textarea class="form-control" type="text" name="Situation" placeholder="Please let us know about your current professional situation as a journalist in your region. We are interested to know about the challenges you, as a journalist, are facing due to the COVID-19 crisis. (maximum 150 words)" cols="50" rows="8" required></textarea></label>
            <label>Types of skills :<textarea class="form-control" type="text" name="Skills" placeholder="We plan to offer skill-building webinars as part of the program. What types of skills would you be interested in advancing for yourself? (maximum 100 words)" cols="50" rows="8" required></textarea></label>
            <p>By clicking “Submit” you agree that you will participate in both parts of the Agora Europe Journalism Grants Program if your application is selected.
              <b>The questions related to the professional situation will enable us to assess the scope of the current crisis in the media sector in Europe. These responses will be anonymized and evaluated.</b><br><br>
              Questions should be directed to Bernadette Geyer, <a href="mailto:agora@hostwriter.org">agora@hostwriter.org</a>.
            </p><br><br>
            <input class="btn btn-lg btn-wide" type="submit" value="Submit">
          </form>
        </div>
      </div>
    </div>
  </section> -->

  <section id="funding" class="pad-lg" style="background-color:#00FFB9">
    <div class="container">
        <div class="row">
          <div class="col-sm-9 col-md-9">
            <h2 class="intro black" style="margin: 20px 0 0 0;"> The Agora Europe Journalism Grants program is made possible with the support of the European Cultural Foundation.</h2>
          </div>
          <div class="col-sm-3 col-md-3">
            <a href="https://www.culturalfoundation.eu/" target="_blank"><img src="/img/ecf-logo.png" style="max-width:180px" /></a>
          </div>
        </div>
      </div>
  </section>
</div>
