---
layout: minimal
title: Agora 2017
permalink: /Agora2017/
---

<div class="about col-sm-10 col-sm-offset-1 text-center">
  <h2>The “Agora 2017 Project” - Cross-border journalism for Europe</h2>

  <p class="intro">Agora 2017 came about through a collaboration between Hostwriter and a team of young change-makers in Armenia. The project team launched a call for applications to which almost 200 journalists applied. Ten journalists from across Europe were selected and invited to the kick-off meeting in Yerevan, Armenia, in March 2017.
</p>

  <h3>The project</h3>
  <h3 class="intro">Why is there no populist party in Portugal? What happens to asylum seekers who have been denied? Will robots steal all our jobs?</h3>
  <p>Ten young journalists from Croatia, Spain, Greece, Germany, Serbia, Italy, Great Britain, Austria, and Portugal teamed up in a temporary newsroom to collaborate on the biggest challenges facing Europe. Over six months, the participants met in Armenia and Sweden and supported each other in a virtual newsroom, offering local insights and feedback.</p>
  
  <p>Each article is published in the participant’s home country (f.e. Süddeutsche Zeitung in Germany and Il dubbio in Italy) and in an English language <a href="{{ site.baseurl }}/Agora2017/Dossier">online dossier</a>. The stories deal with the most pressing European subjects: from migration and citizenship rights, to political polarisation and the effects of care drain as medical professionals in Europe head west.</p>

</div>

<p class="clearfix"></p>

&nbsp; 


<div class="about col-sm-8 col-sm-offset-2 text-center">
 <h3>The team</h3>
 <img src="{{ site.baseurl }}/img/agorateam.jpg">
 <div class="caption">
    <p>Agora 2017 was the brainchild of Hostwriter and change-makers from Armenia.</p>
  </div>
<br />
  <p>
  Agora 2017 was headed by Felix Franz and Abishek Chauhan with the support of Geraldine Cremin, Tabea Grzeszyk and Vanuhi Shushanyan.

The project was funded by Advocate Europe, an annual idea challenge for European initiatives and projects realized by MitOst and Liquid Democracy, funded by Stiftung Mercator to support and to strengthen connection and cohesion in Europe.
</p>
</div>

<p class="clearfix"></p> 



<p class="clearfix"></p> 


&nbsp; 

<div class="row text-center lead-prize" style="color: #00FFB9">
  <br />
  <a href="{{ site.baseurl }}/"><i class="fa fa-arrow-left"></i>  Back to overview</a>
</div>
