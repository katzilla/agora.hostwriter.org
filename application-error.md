---
layout: minimal
title: Agora 2020
permalink: /application-error/
---

<div class="col-sm-12 text-center margin-30">
<h2>Oops!</h2>
<p class="lead-prize">Unfortunately there was a problem. Try it again or reach us via our <a href="https://twitter.com/hostwriter" style="color: #00FFB9">Twitter</a> or
            <a href="https://www.facebook.com/hostwriter.org" style="color: #00FFB9">Facebook.</a></p>
    <br>
    <p class="lead-prize"><a href="http://agora.hostwriter.org" style="color: #00FFB9">Go back to The Agora Project page.</a>
    </div>
