---
layout: minimal
title: Agora 2020
permalink: /application-sent/
---

<div class="col-sm-12 text-center margin-30">
<h2 class="wow fadeIn">Thank you!</h2>
<p class="lead-prize"><b>Thank you for your submission. We'll announce the winners by early-July.</b><br> 
If you have any questions, please email us at <a href="mailto:agora@hostwriter.org">agora@hostwriter.org</a> <br />
    <br>
    <p class="lead-prize"><a href="http://agora.hostwriter.org" style="color: #00FFB9">Go back to The Agora Project page.</a>
    </div>
