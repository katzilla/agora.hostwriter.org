---
layout: minimal
title: Agora 2017 - Dossier
permalink: /Agora2017/Dossier/
---
<div class="home">
  <div class="row">
    <p class="intro text-center col-lg-10 col-lg-offset-1">
      <a href="https://hostwriter.org" target="_blank">Hostwriter</a> and its Armenian partners gave ten journalists from nine different countries the chance to cover 
      European topics from a cross-border perspective. 
      They met in Armenia and Sweden and supported each other with local insights and feedback exchanged through a 
      virtual newsroom. Check out our dossier here.
    </p>
  </div>
  <ul class="post-list">
    {% for post in site.posts %}
      <li class="post col-xs-12 col-sm-6 col-md-4">
        <div class="post-wrapper">
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">
            <div class="row">
              <span class="post-meta col-md-7 col-sm-7 col-xs-6">{{ post.author }}</span>
              <img class="avatar small col-md-5 col-sm-7 col-xs-6 text-right" src="/img/userimg/{{ post.authorimg }}" alt="{{ post.author }}">
            </div>
            <h3> {{ post.teasertitle }} </h3>
            <div class="post-teaser"> {{ post.teaser }}</div>
          </a>
        </div>
      </li>
    {% endfor %}
  </ul>
</div>
